public class Pelicula{
	private String nombre;
	private String director;
	private String categoria;
	private double duracion;
	
	public Pelicula (String nombre,String director,String categoria,double duracion){
		this.nombre=nombre;
		this.director=director;
		this.categoria=categoria;
		this.duracion=duracion;
		}//fin del metodo constructor con parametros
		
	public void setNombre(String nombre){
		this.nombre=nombre;
		}//fin del metodo setNombre
		
	public String getNombre(){
		return nombre;
		}//fin del metodo getNombre
		
	public void setDirector (String director){
		this.director=director;
		}//fin del metoo setDirector
	
	public String getDirector(){
		return director;
		}//fin edl metodo getDirector
		
	public void setCategoria(String categoria){
		this.categoria=categoria;
		}//fin del metodo setCategoria
	
	public String getCategoria(){
		return categoria;
		}//fin del metodo getCategoria
		
	public void setDuracion(double duracion){
		this.duracion=duracion;
		}//fin del metodosetDuracion
		
	public double getDuracion(){
		return duracion;
		}//fin del metodo getDuracion
		
	public String toString(){
    return "Informacion de la pelicula\nNombre: "+getNombre()
      +"\nDirector: "+getDirector()
      +"\nCateoria: "+getCategoria()
      +"\nDuracion: "+getDuracion();
    }//Fin toString
}//Fin de la clase Pelicula
