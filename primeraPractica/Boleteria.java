import javax.swing.JOptionPane;

public class Boleteria{
	public static void main (String arg[]){
		
		char opcion;
		int eleccion, dineroInicial, capacidad, precioEntrada;
		String nombre, director, categoria;
		double duracion;
		
		Pelicula articulo;
		articulo= new Pelicula("SPIDER-MAN: NO WAY HOME","Jon Watts","Accion",1.58);
		
		eleccion= Integer.parseInt(JOptionPane.showInputDialog("Desea ingresar los datos de la pelicula"+"\nDigite 1 para si"+" o 2 para continuar"));
		if (eleccion ==1){
			nombre=JOptionPane.showInputDialog("Digite el nombre de la pelicula");
			articulo.setNombre(nombre);
			director=JOptionPane.showInputDialog("Digite el nombre del director");
			articulo.setDirector(director);
			categoria=JOptionPane.showInputDialog("Digite la categoria de la pelicula");
			articulo.setCategoria(categoria);
			duracion=Double.parseDouble(JOptionPane.showInputDialog("Digite la duracion de la pelicula"));
			articulo.setDuracion(duracion);
			}//fin del if
		
		dineroInicial=Integer.parseInt(JOptionPane.showInputDialog("Cuanto dinero hay en la caja?"));
		Caja caja;
		caja = new Caja(dineroInicial);
	
		precioEntrada=Integer.parseInt(JOptionPane.showInputDialog("Cual es el precio de una entrada?"));
		
		
		Sala sala;
		sala= new Sala(60);
		do{
			capacidad=Integer.parseInt(JOptionPane.showInputDialog("Cuanta capacidad tiene la sala?"));
			if(capacidad>=1 & capacidad<=60){
			sala.setCapacidad(capacidad);
			}else{
				JOptionPane.showMessageDialog(null,"Capacidad digitada invalida ");
			}// del if-else
			}while(capacidad >60); //fin del ciclo dowhile
		
		do{
			opcion= JOptionPane.showInputDialog(
			"* * * * Boleteria* * * *"
			+"\n a. Vender boletos"
			+"\n b. Devolver entrada"
			+"\n c. Ver disponibilidad de espacios"
			+"\n d. Ver informacion de la Pelicula"
			+"\n e. Cerrar boleteria"
			+"\n f. Ver total recaudado"
			+"\n g. Salir").charAt(0);

			switch(opcion){
				case 'a':
					int asientosVendidos,pagoEntradas;
					if(caja!=null){
						asientosVendidos= Integer.parseInt(JOptionPane.showInputDialog("Cuantas entradas desea vender?"));
						sala.aumentarAsientosReservados(asientosVendidos);
						pagoEntradas=precioEntrada*asientosVendidos;
						caja.aumentarRecaudado(pagoEntradas);
					}else{
						JOptionPane.showMessageDialog(null,"La boleteria ya ha sido cerrada");
					}//fin del if-else
				break;
				case'b':
					int asientosDevueltos, devolucionDinero;
					if(caja!=null){
						asientosDevueltos= Integer.parseInt(JOptionPane.showInputDialog("Cuantas entradas desea devolver?"));
						sala.disminuirAsientosReservados(asientosDevueltos);
						devolucionDinero=precioEntrada*asientosDevueltos;
						caja.disminuirRecaudado(devolucionDinero);
					}else{
						JOptionPane.showMessageDialog(null,"La boleteria ya ha sido cerrada");
					}//fin del if-else
				break;
				case 'c':
					if(caja!=null){
						JOptionPane.showMessageDialog(null,sala.getCapacidad());
					}else{
						JOptionPane.showMessageDialog(null,"La boleteria ya ha sido cerrada");
					}
				break;
				case 'd':
					if(caja!=null){
						JOptionPane.showMessageDialog(null,articulo);
					}else{
						JOptionPane.showMessageDialog(null,"La boleteria ya ha sido cerrada");
					}
				break;
				case 'e':
					caja=null;
					JOptionPane.showMessageDialog(null,"Boleteria cerrada");
				break;
				case 'f':
					JOptionPane.showMessageDialog(null,dineroInicial+caja.getRecaudado());
				break;
				case 'g':
					JOptionPane.showMessageDialog(null,"Aplicacion finalizada :)");
				break;
					default:JOptionPane.showMessageDialog(null,"Esa opcion no esta disponible");
				}//fin del swith
		}while(opcion!='g');//fin del ciclo dowhile
	}//fin del main
}//fin de la clase Boleteria
