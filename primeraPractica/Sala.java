public class Sala{
	private int capacidad;
	
	public Sala(int capacidad){
		this.capacidad=capacidad;
		}//fin del metodo contructor con parametros
		
	public void setCapacidad(int capacidad){
		this.capacidad=capacidad;
		}//fin del metodo setCapacidad
		
	public int getCapacidad (){
		return capacidad;
		}//fin del metodo getCapacidad
		
		public int aumentarAsientosReservados(int asientosVendidos){
			capacidad=capacidad-asientosVendidos;
			return capacidad;
		}//fin del metodo aumentarAsientosReservados
		
	public int disminuirAsientosReservados(int asientosDevueltos){
		capacidad=capacidad+asientosDevueltos;
		return capacidad;
	}//fin del metodo disminuirAsientosReservados
		
}//fin de la clase Sala
