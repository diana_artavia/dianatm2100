public class Caja{
	private int recaudado; 
	private int dineroInicial;

	public Caja (int dineroInicial){
		this.dineroInicial=dineroInicial;
	}//fin del metodo constructor con parametros

	public void setRecaudado(int recaudado){
		this.recaudado=recaudado;
	}//fin del metodo setRecaudado

	public int getRecaudado( ){
		return recaudado;
	}//fin el metodo getRecaudado

	public void setDineroInicial(int dineroInicial){
		this.dineroInicial=dineroInicial;
	}//fin del metodo setDineroTotal

	public int getDineroInicial( ){
		return dineroInicial;
	}//fin del metodo getDineroTotal
		
	public int aumentarRecaudado(int pagoEntradas ) {
		recaudado=recaudado+pagoEntradas;
		return recaudado;
	}//fin del metodo aumentarRecaudado

	public int disminuirRecaudado(int devolucionDinero ){
		recaudado=recaudado-devolucionDinero;
		return recaudado;
		}//fin del metodo disminuirRecaudado
	
}//fin de la clase Caja
