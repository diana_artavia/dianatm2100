import javax.swing.JOptionPane;

public class Principal{
	public static void main (String arg[]){
		
		char opcion;
		Computadora computadora;
		computadora=null;	
		
		do{
			opcion= JOptionPane.showInputDialog(
			"* * * *Menu Principal* * * *"
			+"\n a. Crear instancia de computadora sin valores"
			+"\n b. Crear instancia de computadora con los valores necesarios para cada uno de sus atributos"
			+"\n c. Modificar la placa de la computadora"
			+"\n d. Modificar el modelo de la computadora"
			+"\n e. Modificar el tamaño de la pantalla de la computadora."
			+"\n f. Modificar el estado de la computadora"
			+"\n g. Ver los datos de la computadora"
			+"\n h. Salir").charAt(0);
			
			switch(opcion){
				case 'a':			
				if(computadora==null){
					computadora=new Computadora();
					JOptionPane.showMessageDialog(null,"La instancia sin valores ha sido creada");
				}else{
					JOptionPane.showMessageDialog(null,"Opcion invalida, una instancia con valores ya ha sido creada");
				}//fin del if-else
				break;
				case 'b':	
				if(computadora== null){
					computadora= new Computadora("Asus", "98765",23);
					JOptionPane.showMessageDialog(null,"La instancia con valores ha sido creada");
				}else{
					JOptionPane.showMessageDialog(null,"Opcion invalida, una instancia sin valores ya ha sido creada");
				}//fin del if-else
				break;
				case 'c':
				String placa;
				placa= JOptionPane.showInputDialog("Digite la placa de la computadora");
				computadora.setPlaca(placa);
				break;
				case 'd':
				String modelo;
				modelo= JOptionPane.showInputDialog("Digite el modelo de la computadora");
				computadora.setModelo(modelo);
				break;
				case 'e':
				int pantalla;
				pantalla=Integer.parseInt( JOptionPane.showInputDialog("Digite el tamaño de la pantalla de la computadora"));
				computadora.setPantalla(pantalla);
				break;
				case 'f':
				String estado;
				estado=JOptionPane.showInputDialog("Digite el estado de la computadora");
				computadora.setEstado(estado);
				break;
				case 'g':
				JOptionPane.showMessageDialog(null,computadora);
				break;
				case 'h':
				JOptionPane.showMessageDialog(null,"Aplicacion finalizada :)");
				break;
				default: JOptionPane.showMessageDialog(null,"Esa opcion no esta disponible");
				}//fin del switch
			
			}while(opcion!='h');//fin del dowhile
	}//fin del main 
}//fin de la clase Principal
