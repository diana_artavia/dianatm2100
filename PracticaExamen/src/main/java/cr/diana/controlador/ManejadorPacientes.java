/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.diana.controlador;

import cr.diana.modelo.Paciente;
import cr.diana.modelo.RegistroPaciente;
import cr.diana.vista.PanelDatosPaciente;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Dianis
 */
public class ManejadorPacientes implements ActionListener{
    
    private PanelDatosPaciente datosPaciente;
    private RegistroPaciente registro;

    public ManejadorPacientes(PanelDatosPaciente datosPaciente, RegistroPaciente registro) {
        this.datosPaciente = datosPaciente;
        this.registro = registro;
    }
    
    

    @Override
    public void actionPerformed(ActionEvent evento) {
        if (evento.getActionCommand().equals("Guardar")) {
            Paciente paciente = datosPaciente.getPaciente();
            registro.setPaciente(paciente);
            datosPaciente.limpiar();
        } else {
            if (evento.getActionCommand().equals("Buscar")) {
                String cedula = datosPaciente.getCedula();
                int posicion = registro.buscarPaciente(cedula);
                if (posicion != -1) {
                    //JOptionPane.showMessageDialog(null, registro.getSitio(posicion));
                    datosPaciente.setPaciente(registro.getPaciente(posicion));
                } else {
                    JOptionPane.showMessageDialog(null, "No se ha encontrado al paciente");
                }//fin del if/else pocision
            }else{
                if(evento.getActionCommand().equals("Calcular IMC")){
                    double totalIMC;
                    String diagnostico = "";
                    totalIMC = datosPaciente.getPeso() / (datosPaciente.getAltura() * datosPaciente.getAltura());
                    if (totalIMC < 16) {
                        diagnostico = "Criterio de ingreso en hospital";
                        datosPaciente.setDiagnostico(diagnostico);
                    } else {
                        if (totalIMC < 17 & totalIMC > 16.1) {
                            diagnostico = "Infrapeso";
                            datosPaciente.setDiagnostico(diagnostico);
                        } else {
                            if (totalIMC < 18 & totalIMC > 17.1) {
                                diagnostico = "Bajo peso";
                                datosPaciente.setDiagnostico(diagnostico);
                            }else{
                                if (totalIMC < 25 & totalIMC > 18.1) {
                                    diagnostico = "Peso Normal (saludable)";
                                    datosPaciente.setDiagnostico(diagnostico);
                                }else{
                                    if (totalIMC < 30 & totalIMC > 25.1) {
                                        diagnostico = "Sobrepeso (Obesidad de grado I)";
                                        datosPaciente.setDiagnostico(diagnostico);
                                    }else{
                                        if (totalIMC < 35 & totalIMC > 30.1) {
                                            diagnostico = "Sobrepeso Crónico (Obesidad de grado II)";
                                            datosPaciente.setDiagnostico(diagnostico);
                                        }else{
                                            if (totalIMC < 40 & totalIMC > 35.1) {
                                                diagnostico = "Obesidad premórbida (Obesidad de grado III)";
                                                datosPaciente.setDiagnostico(diagnostico);
                                            }else{
                                                if (totalIMC > 40.1) {
                                                    diagnostico = "Obesidad mórbida (obesidad de grado IV)";
                                                    datosPaciente.setDiagnostico(diagnostico);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }else{
                    if (evento.getActionCommand().equals("Salir")) {
                        System.exit(0);
                    }//fin del if
                }//fin del tercer if/else    
            }//fin delsegundo if/else
        }//fin del if/else principal
    }//fin del actionPermormed
    
}//fin de la clase ManejadorPacientes
