/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.diana.modelo;

import java.util.ArrayList;

/**
 *
 * @author Dianis
 */
public class RegistroPaciente {
    private ArrayList<Paciente> listadoPacientes;

    public RegistroPaciente() {
      listadoPacientes= new ArrayList<Paciente>();
    }
    
    public void setPaciente(Paciente paciente){
    listadoPacientes.add(paciente);
    }
    
    public Paciente getPaciente(int posicion){
        return listadoPacientes.get(posicion);
    }
    
    public Paciente getSitio(String nombre){
        int posicion= buscarPaciente(nombre);
      
        if (posicion != -1) {
            return listadoPacientes.get(posicion);
        } else {
            return null;
        }
    }
    
    public void eliminar(int posicion){
        listadoPacientes.remove(posicion);
    }
    
     public int buscarPaciente(String cedula) {
        for (int posicion = 0; posicion < listadoPacientes.size(); posicion++){
            if (listadoPacientes.get(posicion).getCedula().equals(cedula)){
                return posicion;
            }//fin del if
        }//fin del for
        return -1;
    }
     
     
}
