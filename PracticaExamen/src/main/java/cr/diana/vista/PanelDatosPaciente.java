/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.diana.vista;

import cr.diana.modelo.Paciente;

/**
 *
 * @author Dianis
 */
public class PanelDatosPaciente extends javax.swing.JPanel {

    /**
     * Creates new form PanelDatosPaciente
     */
    public PanelDatosPaciente() {
        initComponents();
    }
    
    public Paciente getPaciente(){
       String nombre= jtNombrePaciente.getText();
       String cedula= jtCedula.getText();
       return new Paciente(nombre, cedula);
    }
    
    public void setPaciente(Paciente paciente){
        jtNombrePaciente.setText(paciente.getNombre());
        jtCedula.setText(paciente.getCedula());
    }
    
    public String getCedula(){
        return jtCedula.getText();
    }
    
    public double getPeso(){
      String valor= jtPeso.getText();
      double peso=Double.parseDouble(valor);
      return peso;
    }
    
    public double getAltura(){
        String valor= jtAltura.getText();
        double altura= Double.parseDouble(valor);
        return altura;
    }
    
    public void limpiar(){
        jtNombrePaciente.setText("");
        jtCedula.setText("");
        jtAltura.setText("");
        jtPeso.setText("");
        jtDiagnostico.setText("");
    }
    
    public void setDiagnostico(String diagnostico){
        jtDiagnostico.setText(diagnostico);
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jtNombrePaciente = new javax.swing.JTextField();
        jlNombre = new javax.swing.JLabel();
        jlCedula = new javax.swing.JLabel();
        jtCedula = new javax.swing.JTextField();
        jlAltura = new javax.swing.JLabel();
        jtAltura = new javax.swing.JTextField();
        jlPeso = new javax.swing.JLabel();
        jtPeso = new javax.swing.JTextField();
        jlMetros = new javax.swing.JLabel();
        jlKilogramos = new javax.swing.JLabel();
        jlDiagnostico = new javax.swing.JLabel();
        jtDiagnostico = new javax.swing.JTextField();

        jlNombre.setText("Nombre");

        jlCedula.setText("Cédula");

        jlAltura.setText("Altura");

        jtAltura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtAlturaActionPerformed(evt);
            }
        });

        jlPeso.setText("Peso");

        jlMetros.setText("m");

        jlKilogramos.setText("Kg");

        jlDiagnostico.setText("Diagnóstico");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(62, 62, 62)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlNombre)
                    .addComponent(jlDiagnostico)
                    .addComponent(jlPeso)
                    .addComponent(jlAltura)
                    .addComponent(jlCedula))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jtAltura, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jtPeso, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jlMetros)
                                    .addComponent(jlKilogramos)))
                            .addComponent(jtDiagnostico, javax.swing.GroupLayout.DEFAULT_SIZE, 302, Short.MAX_VALUE))
                        .addContainerGap(62, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jtNombrePaciente, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 302, Short.MAX_VALUE)
                            .addComponent(jtCedula, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlCedula)
                    .addComponent(jtCedula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlNombre)
                    .addComponent(jtNombrePaciente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlAltura)
                    .addComponent(jtAltura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlMetros))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlPeso)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jtPeso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jlKilogramos)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlDiagnostico)
                    .addComponent(jtDiagnostico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(22, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jtAlturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtAlturaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtAlturaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jlAltura;
    private javax.swing.JLabel jlCedula;
    private javax.swing.JLabel jlDiagnostico;
    private javax.swing.JLabel jlKilogramos;
    private javax.swing.JLabel jlMetros;
    private javax.swing.JLabel jlNombre;
    private javax.swing.JLabel jlPeso;
    private javax.swing.JTextField jtAltura;
    private javax.swing.JTextField jtCedula;
    private javax.swing.JTextField jtDiagnostico;
    private javax.swing.JTextField jtNombrePaciente;
    private javax.swing.JTextField jtPeso;
    // End of variables declaration//GEN-END:variables
}
